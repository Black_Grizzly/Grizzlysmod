package grizzly.mod.proxy;

import grizzly.mod.world.OreGeneration;


public class CommonProxy {
	
	public void construct() {
	}
	
	public void setup() {
		OreGeneration.setup();
	}
	
	public void complete() {
	}
	
}