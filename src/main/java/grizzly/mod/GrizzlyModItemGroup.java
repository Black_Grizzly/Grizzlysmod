package grizzly.mod;

import grizzly.mod.lists.BlockList;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class GrizzlyModItemGroup extends ItemGroup
{
	public GrizzlyModItemGroup() 
	{
		super("grizzlyitems");

	}

	@Override
	public ItemStack createIcon() 
	{
		return new ItemStack(Item.BLOCK_TO_ITEM.get(BlockList.uranium_block));
	}

}
