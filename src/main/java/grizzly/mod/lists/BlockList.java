package grizzly.mod.lists;

import net.minecraft.block.Block;

public class BlockList 
{
	// URANIUM
	public static Block uranium_block;
	public static Block uranium_ore, uranium_ore_nether, uranium_ore_end;
	
	// COPPER
	public static Block copper_block;
	public static Block copper_ore, copper_ore_nether, copper_ore_end;
	
	// ALUMINUM
	public static Block aluminum_block;
	public static Block aluminum_ore, aluminum_ore_nether, aluminum_ore_end;
	
	// LEAD
	public static Block lead_block;
	public static Block lead_ore; //, lead_ore_nether, lead_ore_end;
	
	// PLATINUM
	public static Block platinum_block;
	public static Block platinum_ore; //, platinum_ore_nether, platinum_ore_end;
	
	// NICKEL
	public static Block nickel_block;
	public static Block nickel_ore; //, nickel_ore_nether, nickel_ore_end;
	
	// SILVER
	public static Block silver_block;
	public static Block silver_ore; //, silver_ore_nether, silver_ore_end;
	
	
	// IRIDIUM
	public static Block iridium_block;
	public static Block iridium_ore; //, iridium_ore_nether, iridium_ore_end;
	
	// TIN
	public static Block tin_block;
	public static Block tin_ore; //, tin_ore_nether, tin_ore_end;
	
	// STEEL & STAINLESS STEEL
//	public static Block steel_block;
//	public static Block stainless_steel_block;
	
	public static Block reinforced_stone_block;
	public static Block reinforced_glass_block;
//	public static Block reinforced_door;
	

	
}
