package grizzly.mod.lists;


import net.minecraft.item.Item;

public class ItemList
{
	 ///////////////////////
	// MATERIAL "METALS" //
   ///////////////////////
	
	// URANIUM ITEMS (NOT COMPLETE!)
	public static Item uranium;
	public static Item crushed_uranium_ore;
	public static Item purified_crushed_uranium_ore;
	public static Item tiny_pile_of_uranium_235;
	public static Item tiny_pile_of_uranium_238;
	public static Item uranium_235;
	public static Item uranium_238;
	public static Item uranium_dust;
	public static Item uranium_ingot;
	public static Item uranium_block;	
	public static Item uranium_ore;
	public static Item uranium_ore_nether;
	public static Item uranium_ore_end;
	
	// URANIUM TOOLS (NOT COMPLETE!)
	public static Item uranium_pickaxe;
	public static Item uranium_shovel;
	public static Item uranium_axe;
	public static Item uranium_hoe;
	
	// URANIUM WEAPONS (NOT COMPLETE!)
	public static Item uranium_sword;
	
	// URANIUM ARMORY (NOT COMPLETE!)
	public static Item uranium_helmet;
	public static Item uranium_chestplate;
	public static Item uranium_leggings;
	public static Item uranium_boots;
	
	// URANIUM FUEL (NOT COMPLETE!)
	
	
	
	// COPPER ITEMS (NOT COMPLETE!)
	public static Item copper;
	public static Item copper_dust;
	public static Item copper_ingot;
	public static Item copper_plate;
	public static Item copper_cable;
	public static Item copper_item_casing;
	public static Item insulated_copper_cable;
	public static Item copper_block;
	public static Item copper_ore;
	public static Item copper_ore_nether;
	public static Item copper_ore_end;
	
	// COPPER TOOLS (NOT COMPLETE!)
	public static Item copper_pickaxe;
	public static Item copper_shovel;
	public static Item copper_axe;
	public static Item copper_hoe;
	
	// COPPER WEAPONS (NOT COMPLETE!)
	public static Item copper_sword;
	
	// COPPER ARMORY (NOT COMPLETE!)
	public static Item copper_helmet;
	public static Item copper_chestplate;
	public static Item copper_leggings;
	public static Item copper_boots;
	
	// MIXED METALS, DUSTS, INGOTS and MORE
	public static Item mixed_metal_ingot;
	public static Item clay_dust;
	
	// STONE
	public static Item advanced_alloy;
	
	// GOLD ITEMS 
	public static Item gold_dust;
	public static Item gold_cable;
	public static Item gold_plate;
	public static Item gold_item_casing;
	
	// IRON ITEMS (NOT COMPLETE!)
	public static Item iron_dust;
	public static Item iron_plate;
	
	// IRON TOOLS (NOT COMPLETE!)
	public static Item forge_hammer;
	public static Item iron_cutter;

	
		
	// ALUMINUM ITEMS (NOT COMPLETE!)
	public static Item aluminum;
	public static Item aluminum_dust;
	public static Item aluminum_ingot;
	public static Item aluminum_plate;
	
	// ALUMINUM BLOCKS AND ORES (NOT COMPLETE!)
	public static Item aluminum_block;
	public static Item aluminum_ore;
	public static Item aluminum_ore_nether;
	public static Item aluminum_ore_end;
	
	// ALUMINUM TOOLS (NOT COMPLETE!)
	
	// ALUMINUM WEAPONS (NOT COMPLETE!)
	
	// ALUMINUM ARMORY (NOT COMPLETE!)
	
	
	
//	// Lead (NOT COMPLETE!)
	public static Item lead_dust;
	public static Item lead_ingot;
	public static Item lead_block;
	public static Item lead_ore;
	public static Item lead_ore_nether;
	public static Item lead_ore_end;
	
	// Platinum (NOT COMPLETE!)
	public static Item platinum;
	public static Item platinum_dust;
	public static Item platinum_ingot;
	public static Item platinum_block;
	public static Item platinum_ore;
	public static Item platinum_ore_nether;
	public static Item platinum_ore_end;
	
	// Nickel (NOT COMPLETE!)
	public static Item nickel;
	public static Item nickel_dust;
	public static Item nickel_ingot;
	public static Item nickel_block;
	public static Item nickel_ore;
	public static Item nickel_ore_nether;
	public static Item nickel_ore_end;
	
	// Silver (NOT COMPLETE!)
	public static Item silver;
	public static Item silver_dust;
	public static Item silver_ingot;
	public static Item silver_block;
	public static Item silver_ore;
	public static Item silver_ore_nether;
	public static Item silver_ore_end;
	
	// Iridium (NOT COMPLETE!)
	public static Item iridium_unclean;
	public static Item iridium_clean;
	public static Item iridium_dust;
	public static Item iridium_ingot;
	public static Item iridium_block;
	public static Item iridium_ore;
	public static Item iridium_ore_nether;
	public static Item iridium_ore_end;
	
	// Tin (NOT COMPLETE!)
	public static Item tin;
	public static Item tin_dust;
	public static Item tin_ingot;
	public static Item tin_block;
	public static Item tin_ore;
	public static Item tin_ore_nether;
	public static Item tin_ore_end;
	
	// Steel (NOT COMPLETE!)
	public static Item steel;
	public static Item steel_dust;
	public static Item steel_ingot;
	public static Item steel_block;
	public static Item reinforced_stone_block;
	public static Item reinforced_glass_block;
//	public static Item reinforced_door;
	
	
/**
	
	// BUILDING BLOCKS (NOT COMPLETE!)
	// SEEDS AND SEPLINGS (NOT COMPLETE!)
	// TREES AND WOODS (NOT COMPLETE!)
	// FLUIDS AND BUKKETS (NOT COMPLETE!)
	 
**/
	
	
//	// FOODS (NOT COMPLETE!)
	public static Item food_peach;
	public static Item food_special_peach;
	
	
}
