package grizzly.mod.lists;

import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;

public enum ToolMaterialList implements IItemTier
{
	MaterialCopper(8.0f, 6.0f, 800, 2, 22, ItemList.copper_ingot),
	MaterialUranium(12.0f, 9.0f, 1800, 3, 14, ItemList.uranium_ingot),
	MaterialAluminum(8.0f, 6.0f, 700, 2, 22, ItemList.aluminum_ingot),
	MaterialTin(10.0f, 9.0f, 800, 3, 25, ItemList.tin_ingot),
	MaterialNickel(10.0f, 9.0f, 800, 3, 25, ItemList.nickel_ingot),
	MaterialLead(10.0f, 9.0f, 800, 3, 25, ItemList.lead_ingot),
	MaterialSilver(10.0f, 9.0f, 800, 3, 25, ItemList.silver_ingot),
	MaterialPlatinum(10.0f, 9.0f, 800, 3, 25, ItemList.platinum_ingot),
	MaterialIridium(10.0f, 9.0f, 800, 3, 25, ItemList.iridium_ingot),
	MaterialSteel(14.0f, 10.0f, 2400, 3, 14, ItemList.steel_ingot),
	MaterialIron(14.0f, 10.0f, 2400, 3, 14, ItemList.steel_ingot);
//	MaterialStainless_steel(16.0f, 11.0f, 2400, 3, 25, ItemList.stainless_steel_ingot);
	
	/** Damage versus entities. */
	/** The strength of this tool material against blocks which it is effective against. */
	private float attackDamage, efficiency;
	/** "durability" = "getMaxUse" The number of uses this material allows. (wood = 59, stone = 131, iron = 250, diamond = 1561, gold = 32) */
	/** The level of material this tool can harvest (3 = DIAMOND, 2 = IRON, 1 = STONE, 0 = WOOD/GOLD) */
	/** Defines the natural enchantability factor of the material. */
	private int durability, harvestLevel, enchantability;
	private Item repairMaterial;
	
	private ToolMaterialList(float attackDamage, float efficiency, int durability, int harvestLevel, int enchantability, Item repairMaterial)
	{
		this.attackDamage = attackDamage;
		this.efficiency = efficiency;
		this.durability = durability;
		this.harvestLevel = harvestLevel;
		this.enchantability = enchantability;
		this.repairMaterial = repairMaterial;
	}

	@Override
	public int getMaxUses() 
	{
		return this.durability;
	}

	@Override
	public float getEfficiency() 
	{
		return this.efficiency;
	}

	@Override
	public float getAttackDamage() 
	{
		return this.attackDamage;
	}

	@Override
	public int getHarvestLevel() 
	{
		return this.harvestLevel;
	}

	@Override
	public int getEnchantability() 
	{
		return this.enchantability;
	}

	@Override
	public Ingredient getRepairMaterial() 
	{
		return Ingredient.fromItems(this.repairMaterial);
	}
}
