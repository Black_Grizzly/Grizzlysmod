package grizzly.mod.lists;

import grizzly.mod.GrizzlyMod;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public enum ArmourMaterialList implements IArmorMaterial 
{
	MaterialCopper("copper", 400, new int[] {1, 4, 5, 2}, 12, ItemList.copper_ingot, "entity.horse.armor", 0.0f),
	MaterialUranium("uranium", 600, new int[] {1, 3, 5, 2}, 25, ItemList.uranium_ingot, "entity.horse.armor", 0.0f);

	private static final int[] max_damage_array = new int[] { 13, 15, 16, 11 };
	private String name, equipSound;
	private int durability, enchantability;
	private Item repairItem;
	private int[] damageReductionAmounts;
	private float toughness;

	private ArmourMaterialList(String name, int durability, int[] damageReductionAmounts, int enchantability,
			Item repairItem, String equipSound, float toughness) 
	{
		this.name = name;
		this.equipSound = equipSound;
		this.durability = durability;
		this.enchantability = enchantability;
		this.repairItem = repairItem;
		this.damageReductionAmounts = damageReductionAmounts;
		this.toughness = toughness;
	}

	@Override
	public int getDurability(EntityEquipmentSlot slot) 
	{
		return max_damage_array[slot.getIndex()];
	}

	@Override
	public int getDamageReductionAmount(EntityEquipmentSlot slot) 
	{
		return this.damageReductionAmounts[slot.getIndex()] * this.durability;
	}

	@Override
	public int getEnchantability() 
	{
		return this.enchantability;
	}

	@Override
	public SoundEvent getSoundEvent() 
	{
		return new SoundEvent(new ResourceLocation(equipSound));
	}

	@Override
	public Ingredient getRepairMaterial() 
	{
		return  Ingredient.fromItems(this.repairItem);
	}

	@Override
	public String getName() 
	{
		return GrizzlyMod.modid + ":" + this.name;
	}

	@Override
	public float getToughness() 
	{
		return this.toughness;
	}

}
