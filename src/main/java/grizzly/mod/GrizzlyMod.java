package grizzly.mod;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import grizzly.mod.items.ItemCustomAxe;
import grizzly.mod.items.ItemCustomFoods;
import grizzly.mod.items.ItemCustomPickaxe;
import grizzly.mod.items.ItemCustomSpecialFoods;
import grizzly.mod.items.ItemCustomTools;
import grizzly.mod.items.ItemForgeHammer;
import grizzly.mod.lists.ArmourMaterialList;
import grizzly.mod.lists.BlockList;
import grizzly.mod.lists.ItemList;
import grizzly.mod.lists.ToolMaterialList;
import grizzly.mod.proxy.CommonProxy;
import grizzly.mod.proxy.ClientProxy;
import grizzly.mod.world.OreGeneration;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemSword;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
//import net.minecraftforge.registries.IForgeRegistry;

@Mod("grizzlymod")

public class GrizzlyMod 
{
	public static GrizzlyMod instance;
	public static final String modid = "grizzlymod";
	private static final Logger logger = LogManager.getLogger(modid);
		
	public static final ItemGroup grizzlyitems = new GrizzlyModItemGroup();
	public static final ItemGroup grizzlyblocks = new GrizzlyModBlockGroup();
	public static final ItemGroup grizzlytools = new GrizzlyModToolsGroup();
	public static final ItemGroup grizzlyweapons = new GrizzlyModWeaponsGroup();
	public static final ItemGroup grizzlyfoods = new GrizzlyModFoodsGroup();
// 	public static final ItemGroup grizzlyarmory = new GrizzlyModArmoryGroup();
// 	public static final ItemGroup grizzlymachines = new GrizzlyModMachinesGroup();
	
	
	public GrizzlyMod() 
	{
		instance = this;
		
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistries);
		proxy.construct();
		
		MinecraftForge.EVENT_BUS.register(this);
	}

	private final CommonProxy proxy = DistExecutor.runForDist(() -> ClientProxy::new, () -> CommonProxy::new);
	
	public void setup(final FMLCommonSetupEvent event)
	{
		OreGeneration.setupOreGeneration();
		logger.info("Setup method registered.");
		proxy.setup();
	}
	
	private void clientRegistries(final FMLClientSetupEvent event)
	{
		logger.info("clientRegistries method registered.");
	}
	

	public void ready(final FMLLoadCompleteEvent event) 
	{
		proxy.complete();
	}
	
	@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
	public static class RegistryEvents
	{

		
		@SubscribeEvent
		public static void registerItems(final RegistryEvent.Register<Item> event)
		{
			event.getRegistry().registerAll
			(
				
					
				//	ItemList.uranium = new Item(new Item.Properties().group(ItemGroup.MISC)).setRegistryName(location("uranium"))
				//  register("porkchop", new ItemFood(3, 0.3F, true, (new Item.Properties()).group(ItemGroup.FOOD)));
				//  register("bucket", item);
				//  register("water_bucket", new ItemBucket(Fluids.WATER, (new Item.Properties()).containerItem(item).maxStackSize(1).group(ItemGroup.MISC)));
				//  register("lava_bucket", new ItemBucket(Fluids.LAVA, (new Item.Properties()).containerItem(item).maxStackSize(1).group(ItemGroup.MISC)));
					
				
					
					
				// FOOD, SAPPLINGS AND MORE	
					
					
					ItemList.food_peach = new ItemCustomFoods().setRegistryName(location("food_peach")),
					ItemList.food_special_peach = new ItemCustomSpecialFoods().setRegistryName(location("food_special_peach")),
					
				// URANIUM
					ItemList.uranium = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("uranium")),
					ItemList.uranium_235 = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("uranium_235")),
					ItemList.uranium_238 = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("uranium_238")),
					ItemList.crushed_uranium_ore = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("crushed_uranium_ore")),
					ItemList.purified_crushed_uranium_ore = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("purified_crushed_uranium_ore")),
					ItemList.tiny_pile_of_uranium_235 = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("tiny_pile_of_uranium_235")),
					ItemList.tiny_pile_of_uranium_238 = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("tiny_pile_of_uranium_238")),
					ItemList.uranium_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("uranium_dust")),
					ItemList.uranium_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("uranium_ingot")),
					
					ItemList.uranium_axe = new ItemCustomAxe(ToolMaterialList.MaterialUranium, -1.0f, 6.0f, new Item.Properties().group(grizzlytools)).setRegistryName(location("uranium_axe")),
					ItemList.uranium_pickaxe = new ItemCustomPickaxe(ToolMaterialList.MaterialUranium, -2, 6.0f, new Item.Properties().group(grizzlytools)).setRegistryName(location("uranium_pickaxe")),
					ItemList.uranium_shovel = new ItemSpade(ToolMaterialList.MaterialUranium, -3.0f, 6.0f, new Item.Properties().group(grizzlytools)).setRegistryName(location("uranium_shovel")),
					ItemList.uranium_hoe = new ItemHoe(ToolMaterialList.MaterialUranium, 6.0f, new Item.Properties().group(grizzlytools)).setRegistryName(location("uranium_hoe")),
					ItemList.uranium_sword = new ItemSword(ToolMaterialList.MaterialUranium, 0, 6.0f, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("uranium_sword")),
							
					ItemList.uranium_helmet =new ItemArmor(ArmourMaterialList.MaterialUranium, EntityEquipmentSlot.HEAD, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("uranium_helmet")),
					ItemList.uranium_chestplate =new ItemArmor(ArmourMaterialList.MaterialUranium, EntityEquipmentSlot.CHEST, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("uranium_chestplate")),
					ItemList.uranium_leggings =new ItemArmor(ArmourMaterialList.MaterialUranium, EntityEquipmentSlot.LEGS, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("uranium_leggings")),
					ItemList.uranium_boots =new ItemArmor(ArmourMaterialList.MaterialUranium, EntityEquipmentSlot.FEET, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("uranium_boots")),
					
				// COPPER
					ItemList.copper = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("copper")),
					ItemList.copper_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("copper_dust")),
					ItemList.copper_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("copper_ingot")),
					ItemList.copper_plate = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("copper_plate")),
					ItemList.copper_item_casing = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("copper_item_casing")),
					ItemList.copper_cable = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("copper_cable")),
					ItemList.insulated_copper_cable = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("insulated_copper_cable")),
					
					ItemList.copper_axe = new ItemCustomAxe(ToolMaterialList.MaterialCopper, -1.0f, 6.0f, new Item.Properties().group(grizzlytools)).setRegistryName(location("copper_axe")),
					ItemList.copper_pickaxe = new ItemCustomPickaxe(ToolMaterialList.MaterialCopper, -2, 6.0f, new Item.Properties().group(grizzlytools)).setRegistryName(location("copper_pickaxe")),
					ItemList.copper_shovel = new ItemSpade(ToolMaterialList.MaterialCopper, -3.0f, 6.0f, new Item.Properties().group(grizzlytools)).setRegistryName(location("copper_shovel")),
					ItemList.copper_hoe = new ItemHoe(ToolMaterialList.MaterialCopper, 6.0f, new Item.Properties().group(grizzlytools)).setRegistryName(location("copper_hoe")),
					ItemList.copper_sword = new ItemSword(ToolMaterialList.MaterialCopper, 0, 6.0f, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("copper_sword")),
					
					ItemList.copper_helmet =new ItemArmor(ArmourMaterialList.MaterialCopper, EntityEquipmentSlot.HEAD, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("copper_helmet")),
					ItemList.copper_chestplate =new ItemArmor(ArmourMaterialList.MaterialCopper, EntityEquipmentSlot.CHEST, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("copper_chestplate")),
					ItemList.copper_leggings =new ItemArmor(ArmourMaterialList.MaterialCopper, EntityEquipmentSlot.LEGS, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("copper_leggings")),
					ItemList.copper_boots =new ItemArmor(ArmourMaterialList.MaterialCopper, EntityEquipmentSlot.FEET, new Item.Properties().group(grizzlyweapons)).setRegistryName(location("copper_boots")),
					
				// GOLD
					ItemList.gold_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("gold_dust")),
					ItemList.gold_cable = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("gold_cable")),
					ItemList.gold_plate = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("gold_plate")),
					ItemList.gold_item_casing = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("gold_item_casing")),
			
				
				// MIXED METALS, DUSTS, INGOTS and MORE
					ItemList.mixed_metal_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("mixed_metal_ingot")),
					ItemList.advanced_alloy = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("advanced_alloy")),
					ItemList.clay_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("clay_dust")),
					
					
				// IRON
					ItemList.iron_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("iron_dust")),
					ItemList.iron_plate = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("iron_plate")),
					
				// IRON TOOLS
					ItemList.iron_cutter = new ItemCustomTools(ToolMaterialList.MaterialIron, -2, 6.0f, new Item.Properties().group(grizzlytools).rarity(EnumRarity.UNCOMMON)).setRegistryName(location("iron_cutter")),
					ItemList.forge_hammer = new ItemForgeHammer(new Item.Properties().group(grizzlytools)).setRegistryName(location("forge_hammer")),
					
					
				// ALUMINUM
					ItemList.aluminum = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("aluminum")),
					ItemList.aluminum_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("aluminum_dust")),
					ItemList.aluminum_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("aluminum_ingot")),
					ItemList.aluminum_plate = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("aluminum_plate")),
					
//				// Lead
//					ItemList.lead_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("lead_dust")),
//					ItemList.lead_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("lead_ingot")),
//					
//				// Tin
//					ItemList.tin = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("tin")),
//					ItemList.tin_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("tin_dust")),
//					ItemList.tin_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("tin_ingot")),
//					
//				// Nickel
//					ItemList.nickel = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("nickel")),
//					ItemList.nickel_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("nickel_dust")),
//					ItemList.nickel_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("nickel_ingot")),
//					
//				// Iridium
//					ItemList.iridium_unclean = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("iridium_unclean")),
//					ItemList.iridium_clean = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("iridium_clean")),
//					ItemList.iridium_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("iridium_dust")),
//					ItemList.iridium_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("iridium_ingot")),
//					
//				// Platinum
//					ItemList.platinum = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("platinum")),
//					ItemList.platinum_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("platinum_dust")),
//					ItemList.platinum_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("platinum_ingot")),
//					
//				// Silver	
//					ItemList.silver = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("silver")),
//					ItemList.silver_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("silver_dust")),
//					ItemList.silver_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("silver_ingot")),
//					
//				// Steel
//					ItemList.steel = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("steel")),
//					ItemList.steel_dust = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("steel_dust")),
//					ItemList.steel_ingot = new Item(new Item.Properties().group(grizzlyitems)).setRegistryName(location("steel_ingot")),
					
									
				// BLOCKS AND ORES //
					// URANIUM
					ItemList.uranium_block = new ItemBlock(BlockList.uranium_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.uranium_block.getRegistryName()),
					ItemList.uranium_ore = new ItemBlock(BlockList.uranium_ore, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.uranium_ore.getRegistryName()),
					ItemList.uranium_ore_nether = new ItemBlock(BlockList.uranium_ore_nether, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.uranium_ore_nether.getRegistryName()),
					ItemList.uranium_ore_end = new ItemBlock(BlockList.uranium_ore_end, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.uranium_ore_end.getRegistryName()),
					
					// ALUMINUM
					ItemList.aluminum_block = new ItemBlock(BlockList.aluminum_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.aluminum_block.getRegistryName()),
					ItemList.aluminum_ore = new ItemBlock(BlockList.aluminum_ore, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.aluminum_ore.getRegistryName()),
					ItemList.aluminum_ore_nether = new ItemBlock(BlockList.aluminum_ore_nether, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.aluminum_ore_nether.getRegistryName()),
					ItemList.aluminum_ore_end = new ItemBlock(BlockList.aluminum_ore_end, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.aluminum_ore_end.getRegistryName()),
					
					// COPPER
					ItemList.copper_block = new ItemBlock(BlockList.copper_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.copper_block.getRegistryName()),
					ItemList.copper_ore = new ItemBlock(BlockList.copper_ore, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.copper_ore.getRegistryName()),
					ItemList.copper_ore_nether = new ItemBlock(BlockList.copper_ore_nether, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.copper_ore_nether.getRegistryName()),
					ItemList.copper_ore_end = new ItemBlock(BlockList.copper_ore_end, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.copper_ore_end.getRegistryName()),
					
					// LEAD
//					ItemList.lead_block = new ItemBlock(BlockList.lead_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.lead_block.getRegistryName()),
//					ItemList.lead_ore = new ItemBlock(BlockList.lead_ore, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.lead_ore.getRegistryName()),
//					
					// TIN
//					ItemList.tin_block = new ItemBlock(BlockList.tin_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.tin_block.getRegistryName()),
//					ItemList.tin_ore = new ItemBlock(BlockList.tin_ore, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.tin_ore.getRegistryName()),
//					
					// STEEL
//					ItemList.steel_block = new ItemBlock(BlockList.steel_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.steel_block.getRegistryName()),
					ItemList.reinforced_stone_block = new ItemBlock(BlockList.reinforced_stone_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.reinforced_stone_block.getRegistryName()),
					ItemList.reinforced_glass_block = new ItemBlock(BlockList.reinforced_glass_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.reinforced_glass_block.getRegistryName())
//					
//					// SILVER
//					ItemList.silver_block = new ItemBlock(BlockList.silver_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.silver_block.getRegistryName()),
//					ItemList.silver_ore = new ItemBlock(BlockList.silver_ore, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.silver_ore.getRegistryName()),
//										
					// IRIDIUM
//					ItemList.iridium_block = new ItemBlock(BlockList.iridium_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.iridium_block.getRegistryName()),
//					ItemList.iridium_ore = new ItemBlock(BlockList.iridium_ore, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.iridium_ore.getRegistryName()),
//					
					// NICKEL
//					ItemList.nickel_block = new ItemBlock(BlockList.nickel_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.nickel_block.getRegistryName()),
//					ItemList.nickel_ore = new ItemBlock(BlockList.nickel_ore, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.nickel_ore.getRegistryName()),
//					
					// PLATINUM
//					ItemList.platinum_block = new ItemBlock(BlockList.platinum_block, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.platinum_block.getRegistryName()),
//					ItemList.platinum_ore = new ItemBlock(BlockList.platinum_ore, new Item.Properties().group(grizzlyblocks)).setRegistryName(BlockList.platinum_ore.getRegistryName())
			);
			
			
			logger.info("Items registered.");
		}
				
		@SubscribeEvent
		public static void registerBlocks(final RegistryEvent.Register<Block> event)
		{
			event.getRegistry().registerAll
			(
				// BLOCKS
					BlockList.uranium_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(1).sound(SoundType.METAL)).setRegistryName(location("uranium_block")),
					BlockList.copper_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(0).sound(SoundType.METAL)).setRegistryName(location("copper_block")),
					BlockList.aluminum_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(0).sound(SoundType.METAL)).setRegistryName(location("aluminum_block")),
//					BlockList.tin_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(0).sound(SoundType.METAL)).setRegistryName(location("tin_block")),					
//					BlockList.lead_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(0).sound(SoundType.METAL)).setRegistryName(location("lead_block")),
//					BlockList.nickel_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(0).sound(SoundType.METAL)).setRegistryName(location("nickel_block")),
//					BlockList.platinum_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(0).sound(SoundType.METAL)).setRegistryName(location("platinum_block")),
//					BlockList.iridium_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(0).sound(SoundType.METAL)).setRegistryName(location("iridium_block")),
//					BlockList.steel_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(0).sound(SoundType.METAL)).setRegistryName(location("steel_block")),
					BlockList.reinforced_stone_block = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(5.0f, 3.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("reinforced_stone_block")),
					BlockList.reinforced_glass_block = new Block(Block.Properties.create(Material.GLASS).hardnessAndResistance(5.0f, 3.0f).lightValue(0).sound(SoundType.GLASS)).setRegistryName(location("reinforced_glass_block")),
					
				// ORES
					BlockList.uranium_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("uranium_ore")),
					BlockList.uranium_ore_nether = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("uranium_ore_nether")),
					BlockList.uranium_ore_end = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("uranium_ore_end")),
					BlockList.copper_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("copper_ore")),
					BlockList.copper_ore_nether = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("copper_ore_nether")),
					BlockList.copper_ore_end = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("copper_ore_end")),
					BlockList.aluminum_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("aluminum_ore")),
					BlockList.aluminum_ore_nether = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("aluminum_ore_nether")),
					BlockList.aluminum_ore_end = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("aluminum_ore_end"))
//					BlockList.platinum_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("platinum_ore")),
//					BlockList.tin_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("tin_ore")),
//					BlockList.platinum_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("platinum_ore")),
//					BlockList.lead_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("lead_ore")),
//					BlockList.silver_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("silver_ore")),
//					BlockList.nickel_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).sound(SoundType.STONE)).setRegistryName(location("nickel_ore"))
					
			);
			
			
			logger.info("Items registered.");
		}
		
		private static ResourceLocation location(String name)
		{
			return new ResourceLocation(modid, name);
		}
	}
}
