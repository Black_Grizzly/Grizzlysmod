package grizzly.mod;

import grizzly.mod.lists.BlockList;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class GrizzlyModToolsGroup extends ItemGroup
{
	public GrizzlyModToolsGroup() 
	{
		super("grizzlytools");

	}

	@Override
	public ItemStack createIcon() 
	{
		return new ItemStack(Item.BLOCK_TO_ITEM.get(BlockList.uranium_ore));
	}

}
