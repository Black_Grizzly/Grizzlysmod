package grizzly.mod.items;

import java.util.List;

import grizzly.mod.GrizzlyMod;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class ItemCustomSpecialFoods extends ItemFood
{

	public ItemCustomSpecialFoods() 
	{
		super(4, 8, false, new Item.Properties().rarity(EnumRarity.EPIC).group(GrizzlyMod.grizzlyfoods));
		setAlwaysEdible();
		
	}
		

	@OnlyIn(Dist.CLIENT)
	@Override
	public boolean hasEffect(ItemStack stack) 
	{
		return true;
	}
	
	
	@Override
	 protected void onFoodEaten(ItemStack stack, World worldIn, EntityPlayer player) {
	      if (!worldIn.isRemote) 
	      {
	         player.addPotionEffect(new PotionEffect(MobEffects.REGENERATION, 400, 1));
	         player.addPotionEffect(new PotionEffect(MobEffects.RESISTANCE, 6000, 0));
	         player.addPotionEffect(new PotionEffect(MobEffects.FIRE_RESISTANCE, 6000, 0));
	         player.addPotionEffect(new PotionEffect(MobEffects.ABSORPTION, 2400, 3));
	      }
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
	     tooltip.add(new TextComponentString("This is the EPIC PEACH!"));
	}
	
	public ForgeRegistryEntry<Item> rarity(EnumRarity epic) {
		return null;
	}
}
