package grizzly.mod.items;

import grizzly.mod.GrizzlyMod;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemForgeHammer extends Item
{

	public ItemForgeHammer(Properties properties) 
	{
		super(new Properties().group(GrizzlyMod.grizzlytools).defaultMaxDamage(20).rarity(EnumRarity.COMMON));
		
	}
	
	@Override
    public boolean hasContainerItem(ItemStack stack) 
	{
        return true;
    }
	
	@Override
	public ItemStack getContainerItem(ItemStack itemStack) 
	{
		final ItemStack copy = itemStack.copy();
		if (copy.attemptDamageItem(1, Item.random, null));
		{
			copy.shrink(1);
			copy.setDamage(0);
		}
		return copy;
	}
	
}
