package grizzly.mod.items;

import grizzly.mod.GrizzlyMod;
import net.minecraft.init.MobEffects;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class ItemCustomFoods extends ItemFood
{

	public ItemCustomFoods()
	{
		super(4, 8, false, new Item.Properties().group(GrizzlyMod.grizzlyfoods));
		setPotionEffect(new PotionEffect(MobEffects.HEALTH_BOOST, 100, 1, false, true), 1);
	}

	public ForgeRegistryEntry<Item> rarity(EnumRarity epic) {
		return null;
	}

}
