package grizzly.mod.items;

import java.util.List;

import grizzly.mod.GrizzlyMod;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

public class ItemIronCutter extends Item
{

	public ItemIronCutter(Properties properties) 
	{
		super(new Properties().group(GrizzlyMod.grizzlytools));
	}

	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
	     tooltip.add(new TextComponentString("Instantly Broken!"));
	}
		
    @Override
    public boolean hasContainerItem() {
        return true;
    }
    
    @Override
    public boolean hasContainerItem(ItemStack stack) {
        return this.hasContainerItem();
    }
    
    @Override
    public ItemStack getContainerItem(ItemStack itemStack) {
        ItemStack stack = itemStack.copy();
        stack.attemptDamageItem(1, random, null);
        return stack.getDamage() > 0 ? stack:ItemStack.EMPTY;
    }
	
}
