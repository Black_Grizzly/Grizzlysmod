package grizzly.mod;

import grizzly.mod.lists.BlockList;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class GrizzlyModFoodsGroup extends ItemGroup
{
	public GrizzlyModFoodsGroup() 
	{
		super("grizzlyfoods");

	}

	@Override
	public ItemStack createIcon() 
	{
		return new ItemStack(Item.BLOCK_TO_ITEM.get(BlockList.aluminum_ore));
	}

}
