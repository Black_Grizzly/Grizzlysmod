package grizzly.mod.util.interfaces;

public interface IHasModel
{
	public void registerModels();
}