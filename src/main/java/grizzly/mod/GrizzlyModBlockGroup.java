package grizzly.mod;

import grizzly.mod.lists.BlockList;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class GrizzlyModBlockGroup extends ItemGroup
{
	public GrizzlyModBlockGroup() 
	{
		super("grizzlyblocks");

	}

	@Override
	public ItemStack createIcon() 
	{
		return new ItemStack(Item.BLOCK_TO_ITEM.get(BlockList.copper_block));
	}

}
