# GrizzlyMod

![Eclipse](https://img.shields.io/badge/Created%20with-Eclipse%20IDE-orange.svg?style=flat-square&logo=eclipse)

![Forge Version](https://img.shields.io/badge/Forge%20Version-1.13.2--25.0.219-blue.svg?style=flat-square) ![GrizzlyMod Build](https://img.shields.io/badge/Build-Work%20In%20Progress-yellow.svg?style=flat-square) ![GrizzlyMod Version](https://img.shields.io/badge/Version-0.3.1-red.svg?style=flat-square)


## Version 0.3.1
Currently available in version 0.3.1

- a Wiki for the GrizzlyMod is Coming Soon!
  - [GrizzlyMod Wiki](https://gitlab.com/Black_Grizzly/Grizzlysmod/wikis/home)

